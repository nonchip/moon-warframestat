date = require"date"
ffi=require'ffi'
ffi.cdef 'unsigned int sleep(unsigned int seconds);'

Worldstate = require 'worldstate'
ws = Worldstate!

import lpad,rpad,pad from require 'strpad'

notify = (title, text, thumb, timeout=60*1000)->
  return unless title
  title = title\gsub '\'', '\\\''
  text  = text\gsub '\'', '\\\''
  if thumb
    os.execute "notify-send -t #{timeout} -i '#{thumb}' '#{title}' '#{text}'; rm '#{thumb}'"
  else
    os.execute "notify-send -t #{timeout} '#{title}' '#{text}'"

notify_alert = (alert,reason=nil)->
  flags = ''
  flags ..= 'AW ' if alert.mission.archwingRequired
  flags ..= 'NM ' if alert.mission.nightmare
  title = "#{flags}#{alert.mission.node}: #{alert.mission.type}"
  text = "#{alert.mission.faction} #{alert.mission.minEnemyLevel}-#{alert.mission.maxEnemyLevel}"
  text ..= " <i>#{reason}</i>" if reason
  text ..= "<br/>ETA: #{alert.eta}"
  text ..= " | Waves: #{alert.mission.maxWaveNum}" if alert.mission.maxWaveNum
  text ..= "<br/><b>#{alert.mission.reward.asString}</b>"
  thumb = alert.mission.reward.thumbnail
  return title, text, thumb

round = (num, numDecimalPlaces)->
  mult = 10^(numDecimalPlaces or 0)
  math.floor(num * mult + 0.5) / mult

notify_persen = (persen)->
  title = "#{persen.agentType}: #{persen.lastDiscoveredAt}"
  text = "Lv. #{persen.rank} | Hp. #{round(persen.healthPercent*100,2)}%"
  thumb = 'http://content.warframe.com/MobileExport/Lotus/Interface/Icons/Player/Tennogen/PlayerAvatarsSharksteeth.png'
  return title, text, thumb

notify_invasion = (inv)->
  title = "#{inv.node}: #{inv.desc}"
  text = "#{inv.attackingFaction} vs. #{inv.defendingFaction}<br/>"
  if inv.attackerReward and inv.attackerReward.asString~=''
    text ..= "#{inv.attackerReward.asString} / #{inv.defenderReward.asString}<br>"
  else
    text ..= "#{inv.defenderReward.asString}<br>"
  text ..= "#{inv.eta}"
  thumb = 'https://vignette.wikia.nocookie.net/warframe/images/2/26/InvasionSplash.png/revision/latest/scale-to-width-down/128'
  return title, text, thumb


multifilter = (test,...)->
  filter={...}
  filter=filter[1] if type(filter[1])=='table'
  for fitem in *filter
    for titem in *test
      if fitem==titem
        return true,fitem
  return false,nil

seen_alerts={}
handle_alert = (alert)->
  return if alert.expired
  return if seen_alerts[alert.id]
  actDate=date alert.activation
  return if actDate > date(true)\addminutes(1)
  -- see https://github.com/WFCD/warframe-worldstate-parser/blob/master/lib/Reward.js#L86
  important,reason=multifilter alert.rewardTypes, 'oxium','polymerBundle','forma','kavatGene','riven','aura', 'endo'
  if alert.mission.reward.credits > 50000
    important=true
    reason or= '>50kcr'
  if important
    seen_alerts[alert.id]=true
    return notify_alert alert,reason

handle_persen = (persen)->
  return unless persen.isDiscovered
  return notify_persen persen

handle_invasion = (inv)->
  return if inv.completed
  actDate=date inv.activation
  return if actDate > date(true)\addminutes(1)
  return notify_invasion inv

notify_all= (list)->
  text=''
  title=nil
  thumb=nil
  timeout=0
  thumbs={}
  thumbstr=''
  for i in *list
    continue unless #i > 2
    if title
      text ..= "<br/><br/><br/><b>#{i[1]}</b><br/>"
    else
      title = i[1]
    if not thumbs[i[3]]
      thumbu = i[3]\gsub '\'', '\\\''
      thumbs[i[3]]=true
      thumb = os.tmpname!
      thumbstr ..= ' '..thumb
      os.execute "wget -q -O '#{thumb}' '#{thumbu}'"
    text ..= i[2]
    timeout = i[4] if i[4] and i[4]>timeout
  if thumbstr~=''
    thumb = os.tmpname!
    os.execute "/usr/local/bin/montage #{thumbstr} -geometry +0+0 #{thumb}; rm #{thumbstr}"
  notify title, text, thumb, (timeout>1000 and timeout or nil)

wrap_get_thumb= (title,text,thumb,timeout)->
  return unless title
  thumbf = os.tmpname!
  thumb = thumb\gsub '\'', '\\\''
  os.execute "wget -q -O '#{thumbf}' '#{thumb}'"
  return title,text,thumbf,timeout

while true
  if ws.apicode == 200
    notify_all [{handle_alert alert} for alert in *ws.rawdata.alerts]
    notify_all [{handle_persen persen} for persen in *ws.rawdata.persistentEnemies]
    notify_all [{handle_invasion inv} for inv in *ws.rawdata.invasions]
  else
    notify "WFS API error", ws.apistatus
  ffi.C.sleep 60
  ws\poll!