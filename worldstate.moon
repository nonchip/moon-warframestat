get = require 'https_json'

class
  new: (platform='pc')=>
    return nil,'unknown platform' unless platform=='pc' or platform=='ps4' or platform=='xb1'
    @endpoint='https://ws.warframestat.us/'..platform
    @poll!
  poll: =>
    @rawdata, @apicode, @apistatus = get @endpoint
