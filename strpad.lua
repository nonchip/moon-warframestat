local srep = string.rep

-- pad the left side
lpad =
  function (s, l, c)
    local res = srep(c or ' ', l - #s) .. s

    return res
  end

-- pad the right side
rpad =
  function (s, l, c)
    local res = s .. srep(c or ' ', l - #s)

    return res
  end

-- pad on both sides (centering with left justification)
pad =
  function (s, l, c)
    c = c or ' '

    local res1 = rpad(s,    (l / 2) + #s, c) -- pad to half-length + the length of s
    local res2 = lpad(res1,  l,           c) -- right-pad our left-padded string to the full length

    return res2
  end

return {lpad=lpad,rpad=rpad,pad=pad}