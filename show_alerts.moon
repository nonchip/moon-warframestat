Worldstate = require 'worldstate'
import lpad,rpad,pad from require 'strpad'

ws = Worldstate!

for alert in *ws.rawdata.alerts
  continue if alert.expired
  flags=''
  flags..=' AW' if alert.mission.archwingRequired
  flags..=' NM' if alert.mission.nightmare
  line   = rpad alert.mission.node, 25
  line ..= rpad "#{alert.mission.faction} #{alert.mission.type}",30
  line ..= lpad "#{alert.mission.minEnemyLevel}-#{alert.mission.maxEnemyLevel}",5
  line ..= rpad "#{flags}", 10
  line ..= alert.mission.reward.asString
  print line
